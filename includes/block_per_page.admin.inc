<?php
/**
 * @file
 */

/**
 * Implements hook_form()
 */
function block_per_page_settings($form, $form_state) {
  
  // Retrieve a list of content types
  $content_types = _get_content_types();
  
  // Retrieve a list of field
  $fields = _get_fields();

  $form['block_per_page']['block_per_page_content_type'] = array(
    '#type' => 'select',
    '#title' => t('Choose Content type'),
    '#description' => t('Choose the content type in which you will create the nodes that will appear in the block per page'),
    '#options' => $content_types,
    '#default_value' => variable_get('block_per_page_content_type', ''),
  );
  
  $form['block_per_page']['block_per_page_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Block title'),
    '#description' => t('The title of the block as shown to the user.'),
    '#default_value' => variable_get('block_per_page_title', ''),
  );
  
  $form['block_per_page']['block_per_page_pathfield'] = array(
    '#type' => 'select',
    '#title' => t('Field path'),
    '#description' => t('Choose the field in which you will store the block_per_page path'),
    '#options' => $fields,
    '#default_value' => variable_get('block_per_page_pathfield', ''),
  );
  
  return system_settings_form($form);
}

/**
 * Helper function to retrieve all content types in select options array.
 */
function _get_content_types() {
  $content_types = node_type_get_types();
  $ct = array();
  
  foreach ($content_types as $key => $value) {
    $ct[$value->type] = $value->name;
  }
  
  return $ct;
}

/**
 * Helper function to retrieve all available fields.
 */
function _get_fields() {
  $fields = field_info_field_map();
  
  $fd = array();
  
  foreach ($fields as $key => $value) {
    $fd[$key] = $key;
  }

  return $fd;
}